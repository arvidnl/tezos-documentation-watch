#!/bin/bash

set -e

REPOS="metastatedev--tezos,proto-proposal nomadic-labs--tezos,master"
FILES="docs/whitedoc/michelson.rst"

if [ -z $WARN_EMAIL ]; then
   echo "Please set the environment variable WARN_EMAIL before proceeding."
   exit 1
fi

function latest_change {
    REPO_DIR=$1
    BRANCH=$2
    git --git-dir=$REPO_DIR log -n 1 --oneline $BRANCH -- $FILES | cut -d' ' -f1
}

function update_latest_change {
    REPO_DIR=$1
    BRANCH=$2
    echo `latest_change $REPO_DIR $BRANCH` > .latest_$REPO_DIR
}

# latest_change nomadic-labs--tezos/ master
# latest_change metastatedev--tezos/ proto-proposal
# exit

OLDIFS=$IFS
for i in $REPOS; do
    IFS=','
    set -- $i;
    REPO_DIR=$1
    BRANCH=$2

    echo $1 and $2;

    git --git-dir=$REPO_DIR fetch origin $BRANCH

    if [ ! -f .latest_$REPO_DIR ]; then
        update_latest_change $REPO_DIR $BRANCH
    fi

    OLD=`cat .latest_$REPO_DIR`
    CUR=`latest_change $REPO_DIR $BRANCH`

    echo "Old: $OLD, Cur: $CUR"

    if [ "$OLD" != "$CUR" ]; then
        echo 'New changes, notifying $WARN_EMAIL'
        # get log
        LOG=`git --git-dir=$REPO_DIR log --oneline $OLD..$CUR -- $FILES`
        echo $LOG
        # send email
        SUBJECT="New changes on $REPO_DIR:$BRANCH -- $FILES"
        mail -s "$SUBJECT" $WARN_EMAIL <<< $LOG
        # update .latest file
        update_latest_change $REPO_DIR $BRANCH
    else
        echo 'No new changes.'
    fi
done;

IFS=$OLDIFS
