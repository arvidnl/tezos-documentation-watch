#!/bin/sh

set -e

if [ ! -d nomadic-labs--tezos ]; then
    git clone --bare https://gitlab.com/nomadic-labs/tezos.git nomadic-labs--tezos
fi
if [ ! -d metastatedev--tezos ]; then
    git clone --bare https://gitlab.com/metastatedev/tezos.git metastatedev--tezos
fi


echo "Watcher set up!"
